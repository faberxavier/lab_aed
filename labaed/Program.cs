﻿using System;


namespace AulaLab
{
    static class Program
    {

        static void Main()
        {
            recursividade aux = new recursividade();
            Console.WriteLine("Método que calcula enésimo valor recursivo");
            aux.Fibonacci(9);
            Console.WriteLine("Método que armazema série em um vetor");
            aux.Fibonacci2(9);
            Console.WriteLine("Método que armazema série em um vetor sem contaodor");
            aux.Fibonacci3(9);
            Console.WriteLine("pressione para finalizar");
            Console.ReadKey();
        }
    }
}
