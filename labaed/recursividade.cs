﻿using System;


namespace AulaLab
{
    public class recursividade
    {
         int[] valores;
         int cont = 2;
        /*
         MÉTODO DE CHAMADA FIBONACCI PADRÃO
             */
        public void Fibonacci(int n) 
        {
            Console.WriteLine(""+rFibonacci(0,1,n-2));

        }
        /*
         MÉTODO DE CALCULO FIBONACCI PADRÃO
             */
        private int rFibonacci(int z, int v, int n)
        {
            if (n == 1)
            {
                return (z + v);
            }
            return rFibonacci(v, z + v, n - 1);
        }
        /*
         * MÉTODO DE CHAMADA FIBONACCI ARMAZENANDO EM UM VETOR
         */
        public void Fibonacci2(int n)
        {
            valores = new int[n];
            valores [n-1] = rFibonacci2(0, 1, n - 2);
            foreach(var numero in valores)
            {
                Console.WriteLine(numero);
            }
        }
        /*
        * MÉTODO DE CHAMADA FIBONACCI ARMAZENANDO EM UM VETOR
        */
        public void Fibonacci3(int n)
        {
            valores = new int[n];
            valores[n - 1] = rFibonacci3(0, 1,2);
            foreach (var numero in valores)
            {
                Console.WriteLine(numero);
            }
        }
        /*
         * MÉTODO DE CALCULO FIBONACCI ARMAZENANDO EM UM VETOR
         */
        private int rFibonacci2(int z, int v, int n)
        {
            if (n == 1)
            {
                valores[0] = 0;
                valores[1] = 1;
                return (z + v);
            }
            valores[cont] = z + v;
            cont++;
           // valores[n] = z + v;
            return rFibonacci2(v, z + v, n - 1);
        }

        private int rFibonacci3(int z, int v, int n)
        {
            if (n == valores.Length-1)
            {
                valores[0] = 0;
                valores[1] = 1;
                return (z + v);
            }
            Console.WriteLine("Chamada Recursiva fibonacc(" + z + "," + v + "," + n + ")");
            valores[n] = z + v;
            // valores[n] = z + v;
            return rFibonacci3(v, z + v, n + 1);
        }
    }
}
