﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labaed
{
    class ex1
    {
        static void Main()
        {

            
            //CRIANDO OBJETO PARA TRABALHAR COM NÚMEROS PRIMOS             
            prime aux = new prime();
            Console.WriteLine( aux.isPrime(12, 11));
            //CRIANDO VETORES COM OS 1000 PRIMEIROS PRIMOS
            int[] primos = aux.arrayFactory(1000);
            //foreach (int a in primos)
            //{
            //    Console.WriteLine("Numero primo ="+a);
            //}

            //INICIANDO BUSCA BINÁRIA

            //BUSCA BINARIA ITERATIVA 
            //LENDO VALOR A SER PROCURADO
            Console.Write("Digite um número para verificação: ");
            int num = int.Parse(Console.ReadLine());
            //CRIANDO OBJETO DE PESQUISA BINÁRIA
            pesquisa busca = new pesquisa();
            
            //INICIANDO CALCULO DO TEMPO DE EXECUÇÃO 
            var sw = Stopwatch.StartNew();

            //EXECUTANDO PESQUISA
            int[] resultadoBusca = busca.pesquisaIterativa(primos, num);
            //ANALISANDO RESULTADO
            if(num == resultadoBusca[0])
            {
                Console.Write("O número "+num+" é o "+(resultadoBusca[1]+1)+"º valor primo");

            }
            else
            {
                Console.Write("O número " + num + " não é primo. O número primo mais próximo é: " + resultadoBusca[0] + " sendo o "+ (resultadoBusca[1] + 1) + "º valor primo") ;
            }
            //PARANDO CONTAGEM DO TEMPO DE EXECUÇÃO 
            sw.Stop();
            //MEDINDO TEMPO 
            TimeSpan time = sw.Elapsed;
            //EXIBINDO TEMPO DE EXECUÇÃO AO USUÁRIO
            Console.WriteLine("\nTempo Gasto Método Iterativo= "+time);
            //REINICIANDO CALCULO DO TEMPO DE EXECUÇÃO 
            sw = Stopwatch.StartNew();

            //@todo EXECUTAR BUSCA RECURSIVA


            //PARANDO CONTAGEM DO TEMPO DE EXECUÇÃO 
            sw.Stop();
            //MEDINDO TEMPO 
            time = sw.Elapsed;
            //EXIBINDO TEMPO DE EXECUÇÃO AO USUÁRIO
            Console.WriteLine("\nTempo Gasto Método Recursivo= " + time);



            Console.WriteLine("pressione para finalizar");
            Console.ReadKey();
        }
    }
}
