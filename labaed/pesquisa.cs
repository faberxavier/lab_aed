﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labaed
{
    class pesquisa
    {
        // MÉTODO PARA BUSCA DA POSIÇÃO E DO VALOR MAIS PRÓXIMO 
        // <param name="vetor">Vetor com todo os números primos</param>  
        // <param name="target">Valor a ser buscado</param>  
        public int[] pesquisaIterativa(int[] vetor, int target)
        {
            //inicializando retorno
            int[] ret = new int[2];
            //Inicialização de variáveis de controle
            int inicio = 0;            
            int fim = vetor.Length - 1;
            int meio = 0;

            while (inicio <= fim)
            {
                meio = (inicio + fim) / 2;
                //CASO VALOR SEJA MENOR QUE O PIVÔ 
                if (target < vetor[meio])
                {
                    //REDUZIR LIMITE DA BUSCA
                    fim = meio - 1;
                }
                //CASO VALOR SEJA MAIOR QUE O PIVÔ 
                else if (target > vetor[meio])
                {
                    //AMPLIAR ORIGEM DA BUSCA
                    inicio = meio + 1;
                }
                else
                {
                    //VALOR ENCONTRADO RETORNANDO VALOR:POSIÇÃO
                    ret[0] = target;
                    ret[1] = meio;
                    return ret;
                }
            }
            //VALOR NÃO ENCONTRADO RETORNANDO VALOR:POSICAO
            //OBS: MÉTODO RETORNA O VALOR MAIS PRÓXIMO DO OBJETIVO
            ret[0] = target;
            ret[1] = meio;
            return ret;

        }
    }
}

