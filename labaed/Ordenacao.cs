﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estrutura_AED
{
    class Ordenacao
    {
        /// <summary>
        /// 
        /// Nas Region's abaixo estão as Class de cada tipo de ordação.
        /// Nesta estrutura estão cada class separada. Caso queira utiliza 
        /// somente uma estrutura das estrutura de ordenação ou se quiser uma 
        /// melhor organização separando cada estrutura em uma classe.
        /// 
        /// </summary>

        #region REGION - CLASS SHELLSORT
        public class ShellSort
        {
            public void shellSort(int[] arr, int array_size)
            {
                int i, j, inc, temp;
                inc = 3;
                while (inc > 0)
                {
                    for (i = 0; i < array_size; i++)
                    {
                        j = i;
                        temp = arr[i];
                        while ((j >= inc) && (arr[j - inc] > temp))
                        {
                            arr[j] = arr[j - inc];
                            j = j - inc;
                        }
                        arr[j] = temp;
                    }
                    if (inc / 2 != 0)
                        inc = inc / 2;
                    else if (inc == 1)
                        inc = 0;
                    else
                        inc = 1;
                }
            }
            public void ImprimeElemento(int[] arr)
            {
                foreach (var elemeto in arr)
                {
                    Console.WriteLine(elemeto + " ");
                }
                Console.WriteLine("\n");
            }
        }
        #endregion

        #region REGION - CLASS SELECTIONSORT
        class Selection
        {
            private int[] data;
            private static Random generator = new Random();
            //Create an array of 10 random numbers
            public Selection(int size)
            {
                data = new int[size];
                for (int i = 0; i < size; i++)
                {
                    data[i] = generator.Next(20, 90);
                }
            }

            public void Sort()
            {
                Console.Write("\nOrdenando usando Selection :(passo a passo)\n\n");
                display_array_elements();
                int smallest;
                for (int i = 0; i < data.Length - 1; i++)
                {
                    smallest = i;

                    for (int index = i + 1; index < data.Length; index++)
                    {
                        if (data[index] < data[smallest])
                        {
                            smallest = index;
                        }
                    }
                    Swap(i, smallest);
                    display_array_elements();
                }

            }

            public void Swap(int first, int second)
            {
                int temporary = data[first];
                data[first] = data[second];
                data[second] = temporary;
            }

            public void display_array_elements()
            {
                foreach (var element in data)
                {
                    Console.Write(element + " ");
                }
                Console.Write("\n\n");
            }
        }
        #endregion

        #region REGION - CLASS BUBBLESORT
        class BubbleSort
        {
            public static void Main(string[] args)
            {
                int[] a = { 3, 0, 2, 5, -1, 4, 1 };
                int t;
                Console.WriteLine("Original array :");
                foreach (int aa in a)
                    Console.Write(aa + " ");
                for (int p = 0; p <= a.Length - 2; p++)
                {
                    for (int i = 0; i <= a.Length - 2; i++)
                    {
                        if (a[i] > a[i + 1])
                        {
                            t = a[i + 1];
                            a[i + 1] = a[i];
                            a[i] = t;
                        }
                    }
                }
                Console.WriteLine("\n" + "Sorted array :");
                foreach (int aa in a)
                    Console.Write(aa + " ");
                Console.Write("\n");

            }

        }
        #endregion

        #region REGION - CLASS INSERTIONSORT
        class InsertionSort
        {
            public static int[] InsertionSortByShift(int[] inputArray)
            {
                for (int i = 0; i < inputArray.Length - 1; i++)
                {
                    int j;
                    var insertionValue = inputArray[i];
                    for (j = i; j > 0; j--)
                    {
                        if (inputArray[j - 1] > insertionValue)
                        {
                            inputArray[j] = inputArray[j - 1];
                        }
                    }
                    inputArray[j] = insertionValue;
                }
                return inputArray;
            }
        }

        #region REGION - TESTANDO INSERTION
        class TesteInsertion
        {

            static int[] InsertionSort(int[] inputArray)
            {
                for (int i = 0; i < inputArray.Length - 1; i++)
                {
                    for (int j = i + 1; j > 0; j--)
                    {
                        if (inputArray[j - 1] > inputArray[j])
                        {
                            int temp = inputArray[j - 1];
                            inputArray[j - 1] = inputArray[j];
                            inputArray[j] = temp;
                        }
                    }
                }
                return inputArray;
            }
            public static void PrintIntegerArray(int[] array)
            {
                foreach (int i in array)
                {
                    Console.Write(i.ToString() + "  ");
                }
            }

            static void Main(string[] args)
            {
                int[] numbers = new int[10] { 2, 5, -4, 11, 0, 18, 22, 67, 51, 6 };
                Console.WriteLine("\nOriginal Array Elements :");
                PrintIntegerArray(numbers);
                Console.WriteLine("\nSorted Array Elements :");
                PrintIntegerArray(InsertionSort(numbers));
                Console.WriteLine("\n");
            }
        }
        #endregion

        #endregion

        #region REGION - CLASS MERGESORT
        class MergeSortAlgorithm
        {
            public static void MergeSort(int[] input, int low, int high)
            {
                if (low < high)
                {
                    int middle = (low / 2) + (high / 2);
                    MergeSort(input, low, middle);
                    MergeSort(input, middle + 1, high);
                    Merge(input, low, middle, high);
                }
            }

            public static void MergeSort(int[] input)
            {
                MergeSort(input, 0, input.Length - 1);
            }

            private static void Merge(int[] input, int low, int middle, int high)
            {

                int left = low;
                int right = middle + 1;
                int[] tmp = new int[(high - low) + 1];
                int tmpIndex = 0;

                while ((left <= middle) && (right <= high))
                {
                    if (input[left] < input[right])
                    {
                        tmp[tmpIndex] = input[left];
                        left = left + 1;
                    }
                    else
                    {
                        tmp[tmpIndex] = input[right];
                        right = right + 1;
                    }
                    tmpIndex = tmpIndex + 1;
                }

                if (left <= middle)
                {
                    while (left <= middle)
                    {
                        tmp[tmpIndex] = input[left];
                        left = left + 1;
                        tmpIndex = tmpIndex + 1;
                    }
                }

                if (right <= high)
                {
                    while (right <= high)
                    {
                        tmp[tmpIndex] = input[right];
                        right = right + 1;
                        tmpIndex = tmpIndex + 1;
                    }
                }

                for (int i = 0; i < tmp.Length; i++)
                {
                    input[low + i] = tmp[i];
                }
            }

            public static string PrintArray(int[] input)
            {
                string result = String.Empty;

                for (int i = 0; i < input.Length; i++)
                {
                    result = result + input[i] + " ";
                }
                if (input.Length == 0)
                {
                    result = "Array is empty.";
                    return result;
                }
                else
                {
                    return result;
                }
            }
        }
        #endregion

        #region REGION - CLASS HEAPSORT
        class HeapSort
        {
            public static int[] heapSort(int[] vetor)
            {
                buildMaxHeap(vetor);
                int n = vetor.Length;

                for (int i = vetor.Length - 1; i > 0; i--)
                {
                    swap(vetor, i, 0);
                    maxHeapify(vetor, 0, --n);
                }

                return vetor;
            }

            private static void buildMaxHeap(int[] v)
            {
                for (int i = v.Length / 2 - 1; i >= 0; i--)
                {
                    maxHeapify(v, i, v.Length);
                }
            }

            private static void maxHeapify(int[] v, int pos, int n)
            {
                int max = 2 * pos + 1, right = max + 1;
                if (max < n)
                {
                    if (right < n && v[max] < v[right])
                    {
                        max = right;
                    }
                    if (v[max] > v[pos])
                    {
                        swap(v, max, pos);
                        maxHeapify(v, max, n);
                    }
                }
            }
            private static void swap(int[] v, int j, int aposJ)
            {
                int aux = v[j];
                v[j] = v[aposJ];
                v[aposJ] = aux;
            }

        }
        #endregion

        #region REGION - CLASS QUICKSORT
        class QuickSort
        {
            public void QuickSort_Recursive(int[] vetor, int primeiro, int ultimo)
            {

                int baixo, alto, meio, pivo, repositorio;
                baixo = primeiro;
                alto = ultimo;
                meio = (int)((baixo + alto) / 2);

                pivo = vetor[meio];

                while (baixo <= alto)
                {
                    while (vetor[baixo] < pivo)
                        baixo++;
                    while (vetor[alto] > pivo)
                        alto--;
                    if (baixo < alto)
                    {
                        repositorio = vetor[baixo];
                        vetor[baixo++] = vetor[alto];
                        vetor[alto--] = repositorio;
                    }
                    else
                    {
                        if (baixo == alto)
                        {
                            baixo++;
                            alto--;
                        }
                    }
                }

                if (alto > primeiro)
                    QuickSort_Recursive(vetor, primeiro, alto);
                if (baixo < ultimo)
                    QuickSort_Recursive(vetor, baixo, ultimo);
            }
        }

        #region  REGION - TESTANDO QUICKSORT
        class TestandoQuickSort
        {

            static void Main(string[] args)
            {
                QuickSort QS = new QuickSort();
                int[] numeros = { 5, 8, 9, 6, 3, 2, 1, 5, 4 };

                Console.WriteLine("Metódo recursivo QuickSort");

                QS.QuickSort_Recursive(numeros, 0, numeros.Length - 1);

                for (int i = 0; i < 9; i++)
                    Console.WriteLine(numeros[i]);

                Console.ReadLine();
            }
        }
        #endregion


        #endregion

        /// <summary>
        /// 
        /// Nas Region's abaixo estão os metodos de cada tipo de ordação.
        /// Para usar basta usa a class Ordenaca e chamar os metodos dela.
        /// Este modelo é mais pratico pois consegue usar todas as estruturas
        /// de ordenação usando uma classe.
        /// 
        /// </summary>


        #region REGION - METODO INSERTION
        public static int[] insertionSort(int[] vetor)
        {
            int i, j, atual;
            for (i = 1; i < vetor.Length; i++)
            {
                atual = vetor[i];
                j = i;
                while ((j > 0) && (vetor[j - 1] > atual))
                {
                    vetor[j] = vetor[j - 1];
                    j = j - 1;
                }
                vetor[j] = atual;
            }
            return vetor;
        }

        #endregion

        #region REGION - METODO SHELLSORT
        public static int[] shellSort(int[] vetor)
        {
            int h = 1;
            int n = vetor.Length;

            while (h < n)
            {
                h = h * 3 + 1;
            }

            h = h / 3;
            int c, j;
            while (h > 0)
            {
                for (int i = h; i < n; i++)
                {
                    c = vetor[i];
                    j = i;
                    while (j >= h && vetor[j - h] > c)
                    {
                        vetor[j] = vetor[j - h];
                        j = j - h;
                    }
                    vetor[j] = c;
                }
                h = h / 2;
            }
            return vetor;
        }
        #endregion

        #region REGION - METODO SELECTION
        public static int[] selectionSort(int[] vetor)
        {
            int min, aux;

            for (int i = 0; i < vetor.Length - 1; i++)
            {
                min = i;

                for (int j = i + 1; j < vetor.Length; j++)
                    if (vetor[j] < vetor[min])
                        min = j;

                if (min != i)
                {
                    aux = vetor[min];
                    vetor[min] = vetor[i];
                    vetor[i] = aux;
                }
            }

            return vetor;
        }
        #endregion

        #region REGION - METODO HEAPSORT
        public static int[] heapSort(int[] vetor)
        {
            buildMaxHeap(vetor);
            int n = vetor.Length;

            for (int i = vetor.Length - 1; i > 0; i--)
            {
                swap(vetor, i, 0);
                maxHeapify(vetor, 0, --n);
            }

            return vetor;
        }

        private static void buildMaxHeap(int[] v)
        {
            for (int i = v.Length / 2 - 1; i >= 0; i--)
            {
                maxHeapify(v, i, v.Length);
            }
        }

        private static void maxHeapify(int[] v, int pos, int n)
        {
            int max = 2 * pos + 1, right = max + 1;
            if (max < n)
            {
                if (right < n && v[max] < v[right])
                {
                    max = right;
                }
                if (v[max] > v[pos])
                {
                    swap(v, max, pos);
                    maxHeapify(v, max, n);
                }
            }
        }

        private static void swap(int[] v, int j, int aposJ)
        {
            int aux = v[j];
            v[j] = v[aposJ];
            v[aposJ] = aux;
        }

        #endregion

        #region REGION - METODO BUBBLESORT
        public static int[] bubbleSort(int[] vetor)
        {
            int tamanho = vetor.Length;
            int comparacoes = 0;
            int trocas = 0;

            for (int i = tamanho - 1; i >= 1; i--)
            {
                for (int j = 0; j < i; j++)
                {
                    comparacoes++;
                    if (vetor[j] > vetor[j + 1])
                    {
                        int aux = vetor[j];
                        vetor[j] = vetor[j + 1];
                        vetor[j + 1] = aux;
                        trocas++;
                    }
                }
            }

            return vetor;
        } 
        #endregion

        #region REGION - METODO COKTAILSORT
        /*O Cocktail Sort ou Bubble Sort Bidirecional é 
 * uma variação do Bubble Sort que se difere pelo 
 * fato de ordenar a lista em ambas as direções.*/

        public static int[] cocktailSort(int[] vetor)
        {
            int tamanho, inicio, fim, swap, aux;
            tamanho = vetor.Length;
            inicio = 0;
            fim = tamanho - 1;
            swap = 0;
            while (swap == 0 && inicio < fim)
            {
                swap = 1;
                for (int i = inicio; i < fim; i = i + 1)
                {
                    if (vetor[i] > vetor[i + 1])
                    {
                        aux = vetor[i];
                        vetor[i] = vetor[i + 1];
                        vetor[i + 1] = aux;
                        swap = 0;
                    }
                }

                fim = fim - 1;

                for (int i = fim; i > inicio; i = i - 1)
                {
                    if (vetor[i] < vetor[i - 1])
                    {
                        aux = vetor[i];
                        vetor[i] = vetor[i - 1];
                        vetor[i - 1] = aux;
                        swap = 0;
                    }
                }

                inicio = inicio + 1;
            }

            return vetor;
        } 
        #endregion

        #region REGION - METODO QUICKSORT
        public static int[] quickSort(int[] vetor)
        {
            int inicio = 0;
            int fim = vetor.Length - 1;

            quickSort(vetor, inicio, fim);

            return vetor;
        }

        private static void quickSort(int[] vetor, int inicio, int fim)
        {
            if (inicio < fim)
            {
                int p = vetor[inicio];
                int i = inicio + 1;
                int f = fim;

                while (i <= f)
                {
                    if (vetor[i] <= p)
                    {
                        i++;
                    }
                    else if (p < vetor[f])
                    {
                        f--;
                    }
                    else
                    {
                        int troca = vetor[i];
                        vetor[i] = vetor[f];
                        vetor[f] = troca;
                        i++;
                        f--;
                    }
                }

                vetor[inicio] = vetor[f];
                vetor[f] = p;

                quickSort(vetor, inicio, f - 1);
                quickSort(vetor, f + 1, fim);
            }
        } 
        #endregion

    }
}