﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estrutura_AED
{


    #region REGION - LISTA USANDO NODE
    class Node
    {
        public int value;
        public Node prox;

        public Node(int value, Node prox)
        {
            this.value = value;
            this.prox = prox;
        }

        public Node()
        {
            value = 0;
            prox = null;
        }
    }

    class Lista
    {

        Node sentinela;

        public Lista()
        {
            sentinela = new Node();
        }

        public bool isEmpty()
        {
            return (sentinela.prox == null);
        }

        public void insere(int x)
        {
            Node z = new Node(x, sentinela.prox);
            sentinela.prox = z;
        }


        public void retiraUltimo()
        {
            Node ant = this.sentinela;
            Node aux = ant.prox;
            while (aux.prox != null)
            {
                ant = aux;
                aux = aux.prox;
            }
            ant.prox = aux.prox;

        }

        public void imprime()
        {
            Node h = sentinela;
            while (h.prox != null)
            {
                h = h.prox;
                Console.WriteLine(h.value);
            }
        }

    }

    class Teste
    {
        public static void Main(String[] args)
        {
            Lista L = new Lista();
            L.insere(16);
            L.insere(15);
            L.insere(7);
            L.insere(5);
            L.insere(3);
            L.retiraUltimo();
            L.imprime();
            Console.ReadKey();
        }
    }
    #endregion

    #region REGION - LISTA ENCADEADA COM A CLASSE CLIENTE

    #region REGION - CLASS CLIENTE USADA NA LISTA ENCADEADA
    class Cliente
    {
        private string nomeCliente;
        private string cpfCliente;
        private int tipoAtendimento;
        private bool prioritario;
        private int tempoAtendimento;
        internal Cliente proximo;


        #region REGION -  Get Set
        internal string NomeCliente { get => nomeCliente; set => nomeCliente = value; }
        internal string CpfCliente { get => cpfCliente; set => cpfCliente = value; }
        internal int TipoAtendimento { get => tipoAtendimento; set => tipoAtendimento = value; }
        internal bool Prioritario { get => prioritario; set => prioritario = value; }
        internal int TempoAtendimento { get => tempoAtendimento; set => tempoAtendimento = value; }
        #endregion

        #region REGION - Construtor
        public Cliente(string cpfCliente, string nomeCliente, int tipoAtendimento, bool prioritario, int tempoAtendimento)
        {
            this.NomeCliente = nomeCliente;
            this.CpfCliente = cpfCliente;
            this.TipoAtendimento = tipoAtendimento;
            this.Prioritario = prioritario;
            this.TempoAtendimento = tempoAtendimento;
        }
        #endregion
        public Cliente()
        {

        }
        int retornaTempoAtendimento()
        {
            return this.TempoAtendimento;
        }
        internal string retornaCpf()
        {
            return this.CpfCliente;
        }
        public virtual string Imprime()
        {
            return ("VIRTUAL");
        }
    }
    #endregion

    class ListCliente
    {
        Cliente Primeiro;
        internal Cliente Ultimo;
        private int qtd;

        public int Qtd
        {
            get { return qtd; }
            set { qtd = value; }
        }

        public ListCliente()
        {
            Primeiro = new Cliente();
            Ultimo = Primeiro;
            Qtd = 0;
        }
        private Cliente AdicionaCliente(Cliente nova)
        {
            if (nova != null)
            {
                Ultimo.proximo = nova;
                Ultimo = Ultimo.proximo;
                Ultimo.proximo = null;
                return Ultimo;
            }
            else
                return null;
        }
        private Cliente AdicionaClientePrioritario(Cliente nova)
        {
            if (nova != null)
            {
                nova.proximo = Primeiro;
                Primeiro = nova;
                return nova;
            }
            return null;
        }
        public void Adiciona(Cliente nova)
        {
            if (AdicionaCliente(nova) != null)
            {
                Qtd++;
            }
            else
                Console.WriteLine("Não Adicionado!");
        }
        public int ExecutaAtendimento(int tempo, out Cliente clienteManipulado)
        {
            Cliente atual, proximo;
            atual = this.Ultimo;
            if (atual.TempoAtendimento <= tempo)
            {

                proximo = atual.proximo;
                atual = null;
                atual = proximo;
                clienteManipulado = null;
                return 0;
            }
            else
            {
                atual.TempoAtendimento -= tempo;
                clienteManipulado = atual;
                return atual.TempoAtendimento;
            }
        }
        public Cliente BuscarCliente(string NumCPF)
        {
            Cliente aux;

            aux = Primeiro.proximo;

            while (aux != null)
            {
                if (aux.CpfCliente == NumCPF)
                {
                    return aux;
                }
                else
                    aux = aux.proximo;
            }

            return null;
        }

        public void BuscarClientePorNome(string nome)
        {
            Cliente aux;
            aux = Primeiro.proximo;

            while (aux != null)
            {
                if (aux.NomeCliente.CompareTo(nome) == 0)
                {
                    Console.WriteLine(aux.Imprime());
                    aux = aux.proximo;
                }
                else
                    aux = aux.proximo;
            }
        }
        public void ImprimeLista()
        {
            Cliente aux = Primeiro.proximo;
            //string nomeArquivosaida = @"Teste.txt";
            //StreamWriter Arquivo = new StreamWriter(nomeArquivosaida, true, Encoding.UTF8);

            if (aux == null)
            {
                Console.WriteLine("Nenhuma Pessoa Cadastrada");
            }
            else
            {
                while (aux != null)
                {
                    Console.WriteLine(aux.Imprime());
                    aux = aux.proximo;
                }
            }
            //Arquivo.Close();
        }

    } 
    #endregion

}
