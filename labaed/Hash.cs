﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estrutura_AED
{
    /// <summary>
    /// Hash usa a estrutura class ListaCliente que esta dentro da classe Lista.
    /// 
    /// O codigo faz inserção na Hash pelo numero de CPF
    /// 
    /// </summary>

    #region REGION - HASH

    class Hash
    {
        ListCliente[] hash = new ListCliente[200];
        int prim = 31;
        //int posicao = 0;
        public Hash()
        {

        }

        internal void insereHash(Cliente entrada, string posicao)
        {
            ListCliente lista = new ListCliente();
            int aux = retornaPosicao(posicao);
            if (hash[aux] == null)
            {
                lista.Adiciona(entrada);
                hash[aux] = lista;
            }
            else
            {
                hash[aux].Adiciona(entrada);
            }

        }
        int retornaPosicao(string cpf)
        {
            string a = cpf.Substring(8, 3);
            string b = cpf.Substring(12, 2);
            int aux = int.Parse(a + b);
            return aux % prim;
        }
        internal Cliente buscaHash(string entrada)
        {
            int aux = retornaPosicao(entrada);
            if (hash[aux] == null)
            {
                Console.WriteLine("cliente não encontrado!");
                return null;
            }
            else
            {
                return hash[aux].BuscarCliente(entrada);
            }
        }
    } 
    #endregion

}

