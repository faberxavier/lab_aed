using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AED_RandomArrayGenerator
{
    class Program
    {
        const int ARRAYSIZE = 1600000;

        public int[] vetCrescente()
        {
            int[] aux = new int[ARRAYSIZE];
            for(int i=0; i < ARRAYSIZE; i++)
            {
                aux[i] = (i + 1);
            }
            return aux;
        }

        public int[] vetDecrescente()
        {
            int[] aux = new int[ARRAYSIZE];
            for (int i = 0; i < ARRAYSIZE; i++)
            {
                aux[i] = ARRAYSIZE-(i + 1);
            }
            return aux;
        }

        public int[] quaseOrdenado()
        {
            Random aleat = new Random(42);

            int[] aux = new int[ARRAYSIZE];
            for (int i = 0; i < ARRAYSIZE; i++)
            {
                aux[i] = (i + 1);
            }

            for (int i = 0; i < (ARRAYSIZE/20); i++)
            {
                int p1 = aleat.Next(0, ARRAYSIZE);
                int p2 = aleat.Next(0, ARRAYSIZE);
                int temp = aux[p1];
                aux[p1] = aux[p2];
                aux[p2] = temp;
            }
            return aux;
        }

        public int[] vetAleatorio()
        {
            Random aleat = new Random(42);

            int[] aux = new int[ARRAYSIZE];
            for (int i = 0; i < ARRAYSIZE; i++)
            {
                aux[i] = aleat.Next(0, ARRAYSIZE);
            }
            return aux;
        }

        static void Main(string[] args)
        {
        }
    }
}