﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estrutura_AED
{
  
    #region REGION - NODE ARVORE
    class NodeArvore
    {
        public char valor;
        public NodeArvore esquerda, direita/*, pai*/;


        public NodeArvore(char valor, NodeArvore esquerda, NodeArvore direita)
        {
            this.valor = valor;
            this.esquerda = esquerda;
            this.direita = direita;
        }

        public NodeArvore(char valor) : this(valor, null, null) { }
    }
    #endregion

    #region REGION - ARVORE
    class Arvore
    {
        private NodeArvore no;

        public Arvore()
        {
            no = null;
        }


        private NodeArvore inserir(NodeArvore N, char valor)
        {
            if (N == null)
                return new NodeArvore(valor);
            else
            {
                if (valor <= N.valor)
                    N.esquerda = inserir(N.esquerda, valor);
                else if (valor > N.valor)
                    N.direita = inserir(N.direita, valor);
                return N;
            }
        }

        private void imprimir(NodeArvore N)
        {
            if (N == null)
                return;
            //Console.Write(" {0}", N.valor);
            imprimir(N.esquerda);

            Console.Write(" {0}", N.valor);
            imprimir(N.direita);

        }
        public void inserir(char valor)
        {
            no = inserir(no, valor);
        }

        public void imprimir()
        {
            if (no == null)
                Console.WriteLine("\n\tÁrvore inexistente");
            else
                imprimir(no);
        }

    }
    #endregion

    #region REGION - TESTANDO ARVORE
    class TesteArvore
    {
        static void Main(string[] args)
        {

            Arvore folha = new Arvore();

            folha.inserir('A');
            folha.inserir('B');
            folha.inserir('A');
            folha.inserir('C');
            folha.inserir('A');
            folha.inserir('T');
            folha.inserir('E');
            Console.WriteLine("\n\n");
            folha.imprimir();
            Console.ReadKey();
        }
    } 
    #endregion
}
