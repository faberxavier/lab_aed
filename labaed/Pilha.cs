﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estrutura_AED
{

    #region REGION - PILHA
    class Pilha
    {
        private int[] valores;
        private int topo;

        public Pilha(int n) /* Método construtor */
        {
            if (n > 0)
            {
                valores = new int[n];
                topo = -1;
            }
        }

        public int Push(int valor)   /* Método para empilhar */
        {
            if (topo < valores.Length - 1)
            {
                topo++;
                valores[topo] = valor;
                return 0;
            }
            return -1;
        }

        public int Pop() /* Método para desempilhar */
        {
            if (topo >= 0)
            {
                int valor = valores[topo];
                topo--;
                return valor;
            }
            else return -1;  /* Stack Underflow */
        }

        public string ImprimirPilha()    /* Método para imprimir a pilha */
        {
            string saida = "\t";
            if (topo >= 0)
            {
                for (int i = topo; i >= 0; i--)
                {
                    saida = saida + valores[i] + "\n\t";
                }
                return saida;
            }
            else return "\tPilha Vazia";
        }
    }

    namespace ConsoleApplication1
    {
        class TesteLista
        {
            static void Main(string[] args)
            {
                Pilha pilha = new Pilha(5);
                int sair = 0;
                string imprime = "";
                while (sair == 0)
                {
                    imprimeOpcoes();
                    int opcao = int.Parse(Console.ReadLine());
                    if (opcao == 0)
                    {
                        sair = 1;
                    }
                    else
                        if (opcao == 1)
                    {
                        Console.Clear();
                        Console.WriteLine("Digite um numero para inserir na pilha\n");
                        pilha.Push(int.Parse(Console.ReadLine()));
                        imprime = pilha.ImprimirPilha();
                        Console.WriteLine(imprime);
                    }
                    else
                            if (opcao == 2)
                    {
                        Console.Clear();
                        pilha.Pop();
                        imprime = pilha.ImprimirPilha();
                        Console.WriteLine(imprime);
                    }
                    else
                                if (opcao == 3)
                    {
                        Console.Clear();
                        imprime = pilha.ImprimirPilha();
                        Console.WriteLine(imprime);
                    }
                }
            }
            static public void imprimeOpcoes()
            {
                Console.WriteLine("\nEscolha uma opção:\n");
                Console.WriteLine("Sair digite 0");
                Console.WriteLine("Inserir na pilha digite 1");
                Console.WriteLine("Tirar da pilha digite 2");
                Console.WriteLine("Imprimir pilha digite 3\n");
            }
        }
    }

        #endregion

    #region REGION - SEGUNDA OPÇÃO DA PILHA ( FALTA ALGUMAS COISA)
        class PilhaSegundaOp
        {
            private object top = null;
            private Objects objs = new Objects();
            private bool isEmpty = true;
            private int count = 0;

            public bool IsEmpty
            { get { return isEmpty; } }

            public int Count
            { get { return count; } }

            public void Push(object obj)
            {
                objs.Add(obj);
                count++;
                top = obj;
                if (isEmpty)
                    isEmpty = false;
            }

            public object Pop()
            {
                object robj = null;
                if (!isEmpty)
                {
                    robj = top;
                    objs.Remove(count--);
                    if (count > 0)
                        top = objs[count - 1];
                    else
                    {
                        top = null;
                        isEmpty = true;
                    }
                }
                return robj;
            }

            public object Peek()
            { return top; }

            public void Clear()
            {
                objs.Clear();
                isEmpty = true;
                count = 0;
            }

            public object[] ToArray()
            {
                object[] obj = new object[objs.Count];
                for (int i = 0; i < objs.Count; i++)
                {
                    obj[i] = objs[i];
                }
                return obj;
            }
        }

        public class Objects : CollectionBase
        {
            public object this[int item]
            { get { return this.GetObject(item); } }

            public void Add(object obj)
            {
                List.Add(obj);
            }

            public void Clear()
            {
                List.Clear();
            }

            public bool Remove(int index)
            {
                if (index > Count - 1 || index < 0)
                    return false;
                else
                {
                    List.RemoveAt(index);
                    return true;
                }
            }

            private object GetObject(int Index)
            {
                return List[Index];
            }
        }
    #endregion

}
