﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estrutura_AED
{
    #region REGION - FILA
    class Fila
    {
        private int[] valores;
        private int topo;
        private int inicio;

        public Fila(int n) /* Método construtor */
        {
            if (n > 0)
            {
                valores = new int[n];
                topo = -1;
                inicio = 0;
            }
        }

        public int Push(int valor)   /* Método para empilhar */
        {
            if (topo < valores.Length - 1)
            {
                topo++;
                valores[topo] = valor;
                return 0;
            }
            return -1;
        }

        public int Pop() /* Método para desempilhar */
        {
            if (topo >= 0)
            {
                int valor = valores[inicio];
                inicio++;
                return valor;
            }
            else return -1;  /* Stack Underflow */
        }

        public string ImprimirFila()    /* Método para imprimir a fila */
        {
            string saida = "\t";
            if (topo >= 0)
            {
                for (int i = topo; i >= 0; i--)
                {
                    saida = saida + valores[i] + "\n\t";
                }
                return saida;
            }
            else return "\fila Vazia";
        }

        class Program
        {
            static void Main(string[] args)
            {
                Fila fila = new Fila(5);
                int sair = 0;
                string imprime = "";
                while (sair == 0)
                {
                    imprimeOpcoes();
                    int opcao = int.Parse(Console.ReadLine());
                    if (opcao == 0)
                    {
                        sair = 1;
                    }
                    else
                        if (opcao == 1)
                    {
                        Console.Clear();
                        Console.WriteLine("Digite um numero para inserir na fila\n");
                        fila.Push(int.Parse(Console.ReadLine()));
                        imprime = fila.ImprimirFila();
                        Console.WriteLine(imprime);
                    }
                    else
                            if (opcao == 2)
                    {
                        Console.Clear();
                        fila.Pop();
                        imprime = fila.ImprimirFila();
                        Console.WriteLine(imprime);
                    }
                    else
                                if (opcao == 3)
                    {
                        Console.Clear();
                        imprime = fila.ImprimirFila();
                        Console.WriteLine(imprime);
                    }
                }
            }
            static public void imprimeOpcoes()
            {
                Console.WriteLine("\nEscolha uma opção:\n");
                Console.WriteLine("Sair digite 0");
                Console.WriteLine("Inserir na fila digite 1");
                Console.WriteLine("Tirar da fila digite 2");
                Console.WriteLine("Imprimir fila digite 3\n");
            }
        }
    } 
    #endregion
}
