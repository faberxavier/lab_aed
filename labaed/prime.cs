﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labaed
{
    class prime
    {

        // MÉTODO RECURSIVO DE VERIFICAÇÃO SE UM NÚMERO X É PRIMO
        // <param name="num">Número que deverar ser testado</param>  
        // <param name="div">Variável de controle iniciada em num-1</param>  
        public Boolean isPrime(int num, int div)
        {
            //CASO O DIVISOR ATUAL SEJA 1 ==> O NÚMERO É PRIMO
            if(div == 1)
            {
                return true;
            }
            //CASO O DIVISOR ATUAL SEJA DIFERENTE 1 E O RESTO DA DIVISÃO É 0 ==> O NÚMERO NÃO PRIMO
            else if (num%div ==0)
            {
                return false;
            }
            //DIVISOR ATUAL É MAIOR QUE 1 E O RESTO DA DIVISÃO NÃO É 0
            //TESTAR COM DIVISOR-1
            return isPrime(num, div-1);
        }

        // MÉTODO PARA CRIAÇÃO DE UM VETOR COM TODOS OS X NÚMEROS PRIMOS
        // <param name="size">Tamanho do vetor, ou seja, a quantidade de números primos que devem ser encontrados</param>         
        public int[]  arrayFactory (int size)
        {
            //GERANDO VETOR
            int[] array = new int[size];
            //BUSCA INICIANDO DE 2
            int cont = 2;
            //VARIÁVEL PARA CONTROLE DA POSIÇÃO DO VETOR ==> QUANTIDADE DE NÚMEROS PRIMOS ENCONTRADOS ATÉ AGORA
            int pos = 0;
            while(pos < size)
            {
                //TESTE UTILIZANDO A FUÇÃO RECURSIVA
                if (isPrime(cont,cont-1))
                {
                    //ARMAZENANDO VALOR NO ARRAY
                    array[pos] = cont;
                    //ATUALIZANDO POSIÇÃO
                    pos++;
                }
                //BUSCANDO NOVO NÚMERO
                cont++;
            }
            return array;
        }
    }
}
