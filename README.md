<h1>  Documentação Disciplina Laboratório Algorítimos e Estruturas de Dados </h1>

<h2> Ementa</h2>

O objetivo desta disciplina é ensinar ao aluno técnicas de implementação em uma linguagem de programação dos seguintes temas: 

*  Recursividade 
* Referências e apontadores
* Estruturas de dados dinâmicas
* Algoritmos de pesquisa em memória principal
* Árvores binárias
* Tabelas hash
* Ordenação interna
* Complexidade de algoritmos


<h2> Objetivos</h2>

* Auxiliar o aluno a desenvolver habilidades para solução e implementação de problemas com métodos recursivos.
* Reforçar e praticar os conceitos de apontadores, referências e estruturas de dados dinâmicas. 
* Tornar o aluno capaz de implementar e manipular estruturas de dados dinâmicas.
* Tornar o aluno apto a implementar métodos de pesquisa e ordenação em memória principal.
* Realizar práticas envolvendo uso de estruturas de dados e complexidade de algoritmos.

<h2> Unidades de Ensino </h2>


1.  Recursividade

        1.1. Recursividade e algoritmos recursivos

2. Estruturas de dados dinâmicas 

        2.1. Apontadores e referências
      
        2.2. Listas

        2.3. Filas

        2.4. Pilhas

3. Pesquisa em memória principal

        3.1. Pesquisa sequencial e pesquisa binária

        3.2. Árvores binárias
	
        3.3. Tabelas Hash

4. Ordenação interna

        4.1. Algoritmos básicos: bolha, seleção, inserção

        4.2. Heapsort

        4.3. Quicksort
	
        4.4. Práticas de complexidade de algoritmos: estudos de casos com algoritmos de ordenação 

Bibliografia básica

http://ce.bonabu.ac.ir/uploads/30/CMS/user/file/115/EBook/Introduction.to.Algorithms.3rd.Edition.Sep.2010.pdf